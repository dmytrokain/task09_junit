public class Ariphmetic {

    public int add(int a, int b) {
        return a + b;
    }

    public boolean isNumberEven(int number) {
        return number % 2 == 0;
    }

    public boolean isNumberOdd(int number) {
        return number % 2 != 0;
    }

}
