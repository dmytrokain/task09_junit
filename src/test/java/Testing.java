import org.junit.Test;

import static junit.framework.Assert.*;

public class Testing {


    Ariphmetic math = new Ariphmetic();
    
    @Test
    public void testAdding() {

        assertEquals(5, math.add(3, 2));
        assertEquals(5, math.add(5, 0));
        assertEquals(5, math.add(7, -2));
    }
    
    @Test
    public void testNumber() {
        assertTrue("Even", math.isNumberEven(2));
        assertFalse("Not even", math.isNumberEven(3));
        assertTrue("Odd", math.isNumberOdd(3));
        assertFalse("Not odd", math.isNumberOdd(4));
    }
}
